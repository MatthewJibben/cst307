	# Jeff Griffith
	# 
	# Description:  This is an example to illustrate the QtSpim assembler directives
	#
	# Date 9/1/2017
	#
	#

	# Data declarations needed to illustrate my test code
	#
	#


	.data 0x10000000 

	num:	.word	0
	wnum:	.word	42
	hnum:	.half	73
	bnum:	.byte	7
	wans:	.word	0
	hans:	.half	0
	bans:	.byte	0


	#
	# My test program goes into this section
	#

	.text 
	.globl main 
	.ent main


main:	li $t0, 27 	        # load immediate decimal 27 into int reg t0
	sw $t0, num 		# num := t0
	lw $t0, wnum            # t0 := wnum which is equal to 42
	sw $t0, wans		# wans := t0 which from above is 42
	lh $t1, hnum		# t1 := hnum which is 73 and only 16 bits
	sh $t1, hans 		# hans := t1
				

	# All done, terminate the program

	li $v0, 10
	syscall			# all done
