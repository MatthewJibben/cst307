# Author: Matthew Jibben
# Date: 10/14/2017
# Description: test program for qtspim
# This program takes a user input integer, and runs a left logical bitshift on it 7 bits,
# effectively multiplying the input by 2^7

P.1: 	
		.data 0x10000000 
msg1:	.asciiz "Please enter an integer between 1-9: " 
		.text
		.globl main  
main:	addu $s0, $ra, $0 			# storing return address in $s0		
		li $v0, 4 	 				# load syscall 4 to print a string
		la $a0, msg1 	 			# load string address into $a0	
		syscall 					# run the syscall
		li $v0, 5					# load syscall 5 to read an integer
		syscall						# run the syscall
		addu $t0, $v0, $0			# load user input into $t0
		sll $t0, $t0, 7				# bitshift left 7 bits
		li $v0, 1 					# load syscall 1 to print an integer
		addu $a0, $t0, $0 			# load the result into $a0 to be printed
		syscall 					# run the syscall
		addu $ra, $0, $s0			# reload the original return address back into $ra
		jr $ra 						# jump to the return address