#test spim
#ECE 484/584
#

.data 0x10000000 	#global data ges here
N: .word 5		#loop count
X: .word 2, 4, 6, 8, 10	#array of numbers to be added
SUM: .word 0		#location of final sum

str:
.asciiz "The sum of the array is = "

.text			#put program here

.globl main		#global define main
main: lw $s0, N		#load loop counter into $s0
la $t0, X		#load address of X into $t0
and $s1, $s1, $zero	#clear $s1 temp sum
loop: lw $t1, 0($t0)	#load next value of x
add $s1, $s1, $t1	#add it to the running sum
addi $t0, $t0, 4	#increment to the next address
addi $s0, $s0, -1	#decrement loop counter
bne $0, $s0, loop 	#loop back until complete
sw $s1, SUM		#store the final total
li $v0, 10		#syscall to exit cleanly from main only

syscall
.end
